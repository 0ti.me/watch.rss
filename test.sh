#!/usr/bin/env bash

set -e

ERROR_COUNT=0

create-collate-test-input() {
  echo -ne "entry-one-${1}\nentry-two-${1}\n"
}

err() {
  let ERROR_COUNT=${ERROR_COUNT}+1
  >&2 echo "$@"
}

test-collate() {
  local ACTUAL=
  local EXPECTED=
  local INPUTS=()
  local OUTPUT=

  ACTUAL="$(collate --source-file <(create-collate-test-input 1) --source-file <(create-collate-test-input 2))"
  EXPECTED="entry-one-1 entry-one-2\nentry-two-1 entry-two-2"

  decho -e "ACTUAL:\n'${ACTUAL}'"
  decho -e "EXPECTED:\n'${EXPECTED}'"

  diff <(echo "${ACTUAL}") <(echo -e "${EXPECTED}")
}

test-collate-xpaths() {
  local ACTUAL=
  local EXPECTED=
  local STR_CMD=
  local TMP_FILE=
  local XML="<tags><tag><name>name-1</name><link>link-1</link></tag><tag><name>name-2</name><link>link-2</link></tag></tags>"
  local XPATH1="//tags/tag/name/text()"
  local XPATH2="//tags/tag/link/text()"

  TMP_FILE=/tmp/test-collate-xpaths.xml

  echo "${XML}" > "${TMP_FILE}"

  STR_CMD="collate-xpaths"
  STR_CMD="${STR_CMD} --source-file '${TMP_FILE}' --xpath '${XPATH1}' --xpath '${XPATH2}' --wrap-each '\"'"

  decho -e "test-collate-xpaths: CMD='${STR_CMD}'"

  ACTUAL="$(eval "${STR_CMD}")"
  EXPECTED='"name-1" "link-1"\n"name-2" "link-2"'

  decho -e "ACTUAL:\n'${ACTUAL}'"
  decho -e "EXPECTED:\n'${EXPECTED}'"

  decho '< Actual    > Expected'
  diff <(echo "${ACTUAL}") <(echo -e "${EXPECTED}")
}

try() {
  "$@" \
    && >&2 echo "Test success: $@" \
    || err "Test failure: $@"
}

main() {
  local THIS_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

  . "${THIS_DIR}/lib.sh"
  . "${THIS_DIR}/parsers.sh"

  try test-collate
  try test-collate-xpaths

  >&2 echo "ERROR_COUNT=${ERROR_COUNT}"
}

main "$@"
