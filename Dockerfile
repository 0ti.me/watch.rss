FROM alpine

RUN \
  apk update \
  && apk add \
    bash \
    curl \
    libxml2-utils

RUN \
  adduser \
    --shell /bin/bash \
    --disabled-password \
    rss

COPY ./entrypoint.sh /entrypoint.sh
COPY ./lib.sh /lib.sh
COPY ./parsers.sh /parsers.sh

USER rss

ENTRYPOINT [ "/entrypoint.sh" ]
CMD []
