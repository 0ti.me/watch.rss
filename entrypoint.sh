#!/usr/bin/env bash

main() {
  local ARGS=()
  local CHANGES_FILE=
  local COPY_FILE=
  local DOWNLOAD_TORRENTS=0
  local ENABLE_LOGGER=1
  local FILE=
  local LOG_INTERVAL_S="${LOG_INTERVAL_S-10}"
  local LOOP_COUNT=0
  local RAW_FILE=
  local RAW_RESULT=
  local RESULT=
  local SLEEP_INTERVAL_S="${SLEEP_INTERVAL_S-10}"
  local START_TIME=
  local TMP_FILE=
  local TORRENTS_DIR=
  local WATCHLIST_FILE=
  local WATCHLIST_LINE=
  local WATCH_FILTER=
  local WATCH_PARSER=
  local WATCH_PERIOD=
  local WATCH_URL_FILE=
  local WATCH_URL=

  local THIS_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
  . "${THIS_DIR}/lib.sh"

  START_TIME="$(dt)"

  >&2 echo "$0: Info: "
  >&2 echo "    UID=$UID"
  >&2 echo "    START_TIME=${START_TIME}"
  >&2 echo "    args=[$@]"

  while [ $# -gt 0 ]; do
    case ${1} in
      --changes)
        CHANGES_FILE="${2}"
        shift
        ;;
      --changes=*)
        CHANGES_FILE="$(parse-equals --changes "${1}")"
        ;;
      --torrents-dir)
        TORRENTS_DIR="${2}"
        DOWNLOAD_TORRENTS=1
        shift
        ;;
      --torrents-dir=*)
        TORRENTS_DIR="$(parse-equals --torrents-dir "${1}")"
        DOWNLOAD_TORRENTS=1
        ;;
      --watch)
        WATCHLIST_FILE="${2}"
        shift
        ;;
      --watch=*)
        WATCHLIST_FILE="$(parse-equals --watch "${1}")"
        ;;
      *)
        ;;
    esac

    shift
  done

  if [ ${DOWNLOAD_TORRENTS} -eq 1 ] && ! touch "${TORRENTS_DIR}"; then
    >&2 echo "Failed to touch torrents dir, maybe a permissions problem?"

    >&2 echo "USER=${USER}"
    >&2 echo "UID=${UID}"
    >&2 ls -and "${TORRENTS_DIR}"
    >&2 ls -ald "${TORRENTS_DIR}"
  fi

  for FILE in "${WATCHLIST_FILE}" "${CHANGES_FILE}"; do
    if [ -d "${FILE}" ]; then
      rmdir "${FILE}"

      touch "${FILE}"
    fi
  done

  set -e

  if ! [ -f "${WATCHLIST_FILE}" ]; then
    >&2 echo "WATCHLIST_FILE=${WATCHLIST_FILE} was not a file which exists"

    return 1
  fi

  if ! [ -f "${CHANGES_FILE}" ]; then
    >&2 echo "CHANGES_FILE=${CHANGES_FILE} was not a file which exists"

    return 1
  fi

  while true; do
    every WATCH_RSS "${LOG_INTERVAL_S}" \
      && ENABLE_LOGGER=1

    let LOOP_COUNT=${LOOP_COUNT}+1

    WATCH_PARSER=

    rm -f /tmp/watch.rss/random-*
    chown -R 1000:1000 /tmp/watch.rss /watch.rss

    log-if \
      "${ENABLE_LOGGER}" \
      "${0}" \
      "${START_TIME}" \
      "(${LOOP_COUNT}): About to read from WATCHLIST_FILE=${WATCHLIST_FILE}"

    if [ ${DOWNLOAD_TORRENTS} -eq 1 ] \
      && [ "${TORRENTS_DIR}" != "" ] \
      && [ -d "${TORRENTS_DIR}" ]
    then
      download-torrents \
        --changes "${CHANGES_FILE}" \
        --dir "${TORRENTS_DIR}"
    fi

    while read WATCHLIST_LINE; do
      decho "${0}: DOWNLOAD_TORRENTS=${DOWNLOAD_TORRENTS}"
      decho "${0}: TORRENTS_DIR=${TORRENTS_DIR}"

      ARGS=()

      log-if \
        "${ENABLE_LOGGER}" \
        "${0}" \
        "${START_TIME}" \
        "(${LOOP_COUNT}): Processing WATCHLIST_LINE=${WATCHLIST_LINE}"

      WATCH_URL="$(echo "${WATCHLIST_LINE}" | awk '{print $1}' | sed -E 's/^"//;s/"$//')"
      WATCH_PARSER="$(echo "${WATCHLIST_LINE}" | awk '{print $2}' | sed -E 's/^"//;s/"$//')"
      WATCH_PERIOD="$(echo "${WATCHLIST_LINE}" | awk '{print $3}' | sed -E 's/^"//;s/"$//')"
      WATCH_FILTER="$(echo "${WATCHLIST_LINE}" | awk '{print $4}' | sed -E 's/^"//;s/"$//')"
      WATCH_PARSER="${WATCH_PARSER-default}"

#     if [ "${WATCH_FILTER}" != "" ]; then
#       ARGS+=("--filter" "${WATCH_FILTER}")
#     fi

      if [ "${WATCH_PARSER}" != "" ]; then
        ARGS+=("--parser" "${WATCH_PARSER}")
      fi

      if [ "${WATCH_PERIOD}" != "" ]; then
        ARGS+=("--period" "${WATCH_PERIOD}")
      fi

      if [ "${WATCH_URL}" != "" ]; then
        ARGS+=("--url" "${WATCH_URL}")
      fi

      TMP_FILE="$(make-random-tmp-file)"
      ARGS+=("--tmp-file" "${TMP_FILE}")

      if [ "${WATCHLIST_LINE}" != "" ]; then
        ARGS+=("--line" "${WATCHLIST_LINE}")
      fi

      WATCH_URL_FILE="$(get-tmp-watchfile "${WATCH_URL}")"
      log-if \
        "${ENABLE_LOGGER}" \
        "${0}" \
        "${START_TIME}" \
        "(${LOOP_COUNT}): process-url ${ARGS[@]}"
      RAW_RESULT="$(process-url "${ARGS[@]}")"

      if [ -f "${TMP_FILE}" ]; then
        rm "${TMP_FILE}"
      fi

      if [ "${RAW_RESULT}" != "" ]; then
        NEW_FILE="${WATCH_URL_FILE}.new"
        RAW_FILE="$(dirname "${WATCH_URL_FILE}")/.raw.$(basename "${WATCH_URL_FILE}")"
        echo "${RAW_RESULT}" > "${RAW_FILE}"

        if [ "${WATCH_FILTER}" != "" ]; then
          log-if "${ENABLE_LOGGER}" "${0}" "${START_TIME}" "(${LOOP_COUNT}): grep -E '${WATCH_FILTER}' '${RAW_FILE}'"

          grep -E "${WATCH_FILTER}" "${RAW_FILE}"
        else
          log-if "${ENABLE_LOGGER}" "${0}" "${START_TIME}" "(${LOOP_COUNT}): echo '${RAW_RESULT}'"

          echo "${RAW_RESULT}"
        fi | grep -v "XPath set is empty" > "${NEW_FILE}" || >&2 echo -ne

        if [ -f "${WATCH_URL_FILE}" ] \
          && ! diff <(cat "${NEW_FILE}") <(cat "${WATCH_URL_FILE}") >> "${CHANGES_FILE}"
        then
          log-if \
            "${ENABLE_LOGGER}" \
            "${0}" \
            "${START_TIME}" \
            "(${LOOP_COUNT}): A difference was detected above for ${WATCH_URL_FILE}"
        fi

        mv "${NEW_FILE}" "${WATCH_URL_FILE}"
      else
        log-if "${ENABLE_LOGGER}" "${0}" "${START_TIME}" "(${LOOP_COUNT}): touch '${WATCH_URL_FILE}'"

        touch "${WATCH_URL_FILE}"
      fi
    done < <(cat "${WATCHLIST_FILE}" | escaped-newlines | sort | uniq)

    log-if \
      "${ENABLE_LOGGER}" \
      "${0}" \
      "${START_TIME}" \
      "(${LOOP_COUNT}): Sleeping for about ${SLEEP_INTERVAL_S} seconds"

    sleep "${SLEEP_INTERVAL_S}"

    ENABLE_LOGGER=0
  done
}

if [[ "${BASH_SOURCE[0]}" == "$0" ]]; then
  main "$@"
fi
