# Watch.rss

Allows monitoring an RSS feed for changes, explicitly intended for torrents.

## First run

    mkdir mnt-watch-rss

    touch mnt-watch-rss/watchlist

### Add a search rss (i.e. add any of these torrents without filtering)

    echo 'YOUR_URL_HERE' >> mnt-watch-rss/watchlist

### Add a catchall rss (i.e. add only specific torrents from this list)

    echo 'YOUR_URL_HERE "" 600 (YOUR_FILTER_HERE)' >> mnt-watch-rss/watchlist

## Example watchlist:

### Uses defaults

    https://chilebt.com/rss/{rss-id-removed}

### Multiline useful example

* $1 is the URL to use
* $2 is the parser, defaulted because of an empty string
* $3 is the frequency in seconds to search this URL
* $4 is the regex to use, you can do multi-line using the method below

```
https://iptorrents.com/t.rss?u={userid-removed};tp={tp-removed} \
  "" \
  600 \
(American.?Gods.*S03)|\
(American.?Gods.*S04)
```
