#!/usr/bin/env bash

chilebt() {
  local ARGS=()
  local FILTER=
  local LINE=
  local SOURCE_FILE=
  local THIS_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

  . "${THIS_DIR}/lib.sh"

  while [ $# -gt 0 ]; do
    case $1 in
      --filter)
        FILTER="${2}"
        shift
        ;;
      --filter=*)
        FILTER="$(parse-equals --filter "${1}")"
        ;;
      --line)
        LINE="${2}"
        shift
        ;;
      --line=*)
        LINE=("$(parse-equals --line "${1}")")
        ;;
      --source-file)
        SOURCE_FILE="${2}"

        shift
        ;;
      --source-file=*)
        SOURCE_FILE+=("$(parse-equals --source-file "${1}")")
        ;;
      *)
        >&2 echo "chilebt: Unsupported arg $1"
        return 1
        ;;
    esac

    shift
  done

  if [ "${SOURCE_FILE}" = "" ] || ! [ -f "${SOURCE_FILE}" ]; then
    >&2 echo "chilebt: Missing SOURCE_FILE=${SOURCE_FILE}"

    return 1
  fi

  ARGS=( \
    "collate-xpaths" \
    "--source-file" "${SOURCE_FILE}" \
    "--xpath" "//rss/channel/item/title/text()" \
    "--xpath" "//rss/channel/item/link/text()" \
    "--wrap-each" "\"" \
  )

  if [ "${FILTER}" != "" ]; then
    ARGS+=("--filter" "${FILTER}")
  fi

  decho "${ARGS[@]}"
  "${ARGS[@]}"
}

default() {
  chilebt "$@"
}
