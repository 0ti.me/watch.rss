#!/usr/bin/env bash

set -e

brief-diff() {
  diff "${@}"
}

check-success-date() {
  local AT_LEAST_SECONDS=
  local DATE_FILE=
  local SUCCESS_DATE=
  local URL=

  while [ $# -gt 0 ]; do
    case $1 in
      --at-least-seconds)
        AT_LEAST_SECONDS="${2}"
        shift
        ;;
      --at-least-seconds=*)
        AT_LEAST_SECONDS="$(parse-equals --at-least-seconds "${1}")"
        ;;
      --url)
        URL="${2}"
        shift
        ;;
      --url=*)
        URL="$(parse-equals --url "${1}")"
        ;;
      *)
        >&2 echo "check-success-date: Unsupported arg $1"
        ;;
    esac

    shift
  done

  if [ "${URL}" = "" ]; then
    >&2 echo "check-success-date: Missing the expected URL arg, got '${URL}'"

    return 1
  fi

  if [ "${AT_LEAST_SECONDS}" = "" ]; then
    >&2 echo "check-success-date: Missing the expected AT_LEAST_SECONDS arg, got '${AT_LEAST_SECONDS}'"

    return 1
  fi

  DATE_FILE="$(get-tmp-datefile "${URL}")"

  if [ -f "${DATE_FILE}" ]; then
    SUCCESS_DATE="$(cat "${DATE_FILE}")"
  fi

  if [ "${SUCCESS_DATE}" = "" ]; then
    SUCCESS_DATE=0
  fi

  decho "check-success-date: \$(( $(dt) - ${SUCCESS_DATE} ))"
  decho "check-success-date: $(( $(dt) - ${SUCCESS_DATE} )) -gt ${AT_LEAST_SECONDS}"

  if [ "$(( $(dt) - ${SUCCESS_DATE} ))" -gt "${AT_LEAST_SECONDS}" ]; then
    decho "check-success-date: Time expired"

    return 0
  else
    decho "check-success-date: Time not up yet"

    return 1
  fi
}

collate() {
  local CONTINUE=1
  local FD=0
  local FILTER=
  local FIRST=1
  local I=0
  local LINE=
  local SOURCE=
  local SOURCES=()
  local WRAP_EACH=

  while [ $# -gt 0 ]; do
    case $1 in
      --filter)
        FILTER="${2}"
        shift
        ;;
      --filter=*)
        FILTER="$(parse-equals --filter "${1}")"
        ;;
      --source-file)
        SOURCES+=("${2}")
        shift
        ;;
      --source-file=*)
        SOURCES+=("$(parse-equals --source-file "${1}")")
        ;;
      --wrap-each)
        WRAP_EACH="${2}"
        shift
        ;;
      --wrap-each=*)
        WRAP_EACH="$(parse-equals --wrap-each "${1}")"
        ;;
      *)
        if [ -f "$1" ]; then
          SOURCES+=("$1")
        else
          >&2 echo "collate: Unsupported arg $1"
          return 1
        fi
        ;;
    esac

    shift
  done

  for (( I=0 ; I<${#SOURCES[@]} ; ++I )); do
    SOURCE="${SOURCES[${I}]}"
    let FD=$(( ${I} + 100 ))

    decho "collate: eval 'exec ${FD}<${SOURCE}'"

    eval "exec ${FD}<${SOURCE}"
  done

  while [ ${CONTINUE} -eq 1 ]; do
    CONTINUE=0
    FIRST=1

    for (( I=0 ; I<${#SOURCES[@]} ; ++I )); do
      SOURCE="${SOURCES[${I}]}"
      let FD=$(( ${I} + 100 ))

      read LINE <&${FD} \
        && decho "Read '${LINE}' from '${SOURCE}'" \
        || decho "Failed to read a line from SOURCE=${SOURCE}"

      if [ "${LINE}" != "" ]; then
        CONTINUE=1

        if [ "${FIRST}" -eq 1 ]; then
          FIRST=0
        else
          echo -ne " "
        fi

        echo -ne "${WRAP_EACH}${LINE}${WRAP_EACH}"
      fi
    done

    echo -ne "\n"
  done
}

collate-xpaths() {
  local CMD=
  local DEST_FILE=
  local FILES=()
  local FILTER=
  local SOURCE_FILE=
  local XPATH=
  local XPATHS=()
  local WRAP_EACH=

  local THIS_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
  . "${THIS_DIR}/lib.sh"

  decho "collate-xpaths: \$\#: $#"

  while [ $# -gt 0 ]; do
    decho "collate-xpaths: \$1=$1"
    case $1 in
      --filter)
        FILTER="${2}"
        shift
        ;;
      --filter=*)
        SOURCE_FILE="$(parse-equals --filter "${1}")"
        ;;
      --source-file)
        SOURCE_FILE=("${2}")
        shift
        ;;
      --source-file=*)
        SOURCE_FILE="$(parse-equals --source-file "${1}")"
        ;;
      --wrap-each)
        WRAP_EACH="${2}"
        shift
        ;;
      --wrap-each=*)
        WRAP_EACH="$(parse-equals --wrap-each "${1}")"
        ;;
      --xpath)
        XPATHS+=("${2}")
        shift
        ;;
      --xpath=*)
        XPATHS+=("$(parse-equals --xpath "${1}")")
        ;;
      *)
        >&2 echo "collate-xpaths: Unsupported arg $1"
        return 1
        ;;
    esac

    shift
  done

  decho "collate-xpaths: \${#XPATHS[@]} ${#XPATHS[@]}"

  if [ ${#XPATHS[@]} -lt 1 ]; then
    >&2 echo expected at least one xpath

    return 1
  fi

  if [ "${SOURCE_FILE}" = "" ]; then
    >&2 echo "expected SOURCE_FILE='${SOURCE_FILE}' to be a file"

    return 1
  fi

  CMD="collate"

  for XPATH in "${XPATHS[@]}"; do
    DEST_FILE="$(make-random-tmp-file)"
    FILES+=("${DEST_FILE}")

    xmllint --xpath "${XPATH}" "${SOURCE_FILE}" >"${DEST_FILE}" 2>&1 \
      || >&2 echo -ne

    CMD="${CMD} --source-file ${DEST_FILE}"
  done

  if [ "${WRAP_EACH}" != "" ]; then
    CMD+=" --wrap-each '${WRAP_EACH}'"
  fi

  if [ "${FILTER}" = "" ]; then
    decho "collate-xpaths: CMD='${CMD}'"
    eval "${CMD}" || >&2 echo -ne
    CODE=${PIPESTATUS[0]}
  else
    decho "collate-xpaths: CMD='${CMD} | grep -iE '${FILTER}''"
    eval "${CMD}" | grep -E "${FILTER}" \
      || >&2 echo -ne
    CODE=${PIPESTATUS[0]}
  fi

  decho "PIPESTATUS[0]=${CODE}"

  for DEST_FILE in "${FILES[@]}"; do
    decho "collate-xpaths: rm -f '${DEST_FILE}'"
    rm -f "${DEST_FILE}"
  done

  return "${CODE}"
}

decho() {
  if [ ${DEBUG-0} -eq 1 ]; then
    >&2 echo "$@"
  fi
}

download-torrents() {
  local CHANGES_FILE=
  local DIR=
  local LINE=
  local OUTPUT_FILE=
  local TARGET=
  local URL=

  while [ $# -gt 0 ]; do
    case $1 in
      --changes)
        CHANGES_FILE="${2}"
        shift
        ;;
      --changes=*)
        CHANGES_FILE="$(parse-equals --changes "${1}")"
        ;;
      --dir)
        DIR="${2}"
        shift
        ;;
      --dir=*)
        DIR="$(parse-equals --dir "${1}")"
        ;;
      *)
        >&2 echo "download-torrents: Unsupported arg $1"
        ;;
    esac

    shift
  done

  if [ "${CHANGES_FILE}" = "" ] || ! [ -f "${CHANGES_FILE}" ] || ! [ -s "${CHANGES_FILE}" ]; then
    decho "download-torrents: Skipping processing of CHANGES_FILE"

    return 0
  fi

  if [ "${DIR}" = "" ] || ! [ -d "${DIR}" ]; then
    >&2 echo "download-torrents: --dir arg \${DIR}=${DIR} was not a directory as expected"

    return 1
  fi

  >&2 echo "download-torrents: Processing changes file"

  TARGET="$(make-random-tmp-file)"
  cp "${CHANGES_FILE}" "${TARGET}"
  truncate -s 0 "${CHANGES_FILE}"

  set +e

  while read LINE; do
    while read URL; do
      OUTPUT_FILE="${DIR}/$(basename "${URL}" | sed 's/?.*//')"
      echo "download-torrents: curl --no-progress-meter -o '${OUTPUT_FILE}' '${URL}?"
      curl --no-progress-meter -o "${OUTPUT_FILE}" "${URL}"
      CODE=$?

      if [ ${CODE} -ne 0 ]; then
        mv "${TARGET}" "${CHANGES_FILE}"

        set -e

        return "${CODE}"
      fi
    done < <(echo "${LINE}" | grep -o "[^ \"]*//[^ \"]*")
  done < "${TARGET}"

  rm -f "${TARGET}"
}

dt() {
  date --utc +%s
}

each() {
  local CMD=
  local LINE=
  local THIS_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

  while [ $# -gt 0 ]; do
    case $1 in
      *)
        CMD+=" ${1}"
        ;;
    esac

    shift
  done

  while read LINE; do
    eval "${CMD}" "${LINE}"
  done
}

escaped-newlines() {
  local LINE=

  while read LINE; do
    echo "${LINE}"
  done
}

every_date() {
  dt
}

every_usage() {
  local U="${BASH_SOURCE[0]}"
  local B="$(basename "${U}")"

  >&2 echo -ne \
    "${B} Usage:\n" \
    "\t${U} <unique-identifier> <time-in-ms>\n" \
    "\tEVERY_IDENTIFIER=<unique-identifier> ${U} <time-in-ms>\n"
}

every() {
  local DIR="${DIR-/tmp/.every}"
  local EVERY_IDENTIFIER=${EVERY_IDENTIFIER}
  local FILE=
  local HELP=0
  local LAST=0
  local NEXT=
  local NOW=
  local TIME=

  for i in "$@"; do
    case $i in
      -*h*)
        HELP=1
        ;;
      -*help*)
        HELP=1
        ;;
      *)
        ;;
    esac
  done

  if [ ${HELP} -eq 1 ] || [ $# -eq 0 ] || ( [ $# -eq 1 ] && [ "${EVERY_IDENTIFIER}" = "" ] ); then
    every_usage

    return 0
  elif [ $# -eq 2 ]; then
    EVERY_IDENTIFIER="${1}"
    shift
  fi

  TIME="${1}"
  shift

  FILE="${DIR}/${EVERY_IDENTIFIER}.every.last"

  mkdir -p "${DIR}"

  if ! [ -e "${FILE}" ]; then
    echo 0 > "${FILE}"
  elif ! [ -f "${FILE}" ]; then
    every_usage "${FILE} exists and is not a file"

    return -1
  fi

  LAST="$(cat "${FILE}" | head -n 1)"
  let NEXT=${LAST}+${TIME}
  NOW="$(every_date)"

  if [ ${DEBUG:-0} -eq 1 ]; then
    >&2 echo "${NOW} -gt ${NEXT} (${LAST}+${TIME})?"
  fi

  if [ "${NOW}" -gt "${NEXT}" ]; then
    echo "${NOW}" > "${FILE}"

    return 0
  else
    return 1
  fi
}

format-uptime() {
  local HOURS=0
  local MINUTES=0
  local SECONDS=0
  local START_TIME="${1}"
  local UPTIME=$(( $(dt) - ${START_TIME} ))

  set +e

  while [ ${UPTIME} -ge 3600 ]; do
    let UPTIME=${UPTIME}-3600
    let HOURS=${HOURS}+1
  done

  while [ ${UPTIME} -ge 60 ]; do
    let UPTIME=${UPTIME}-60
    let MINUTES=${MINUTES}+1
  done

  SECONDS=${UPTIME}

  set -e

  if [ ${HOURS} -gt 0 ]; then
    echo ${HOURS} ${MINUTES} ${SECONDS} | awk '{printf("(%02dh.%02dm.%02ds uptime)", $1, $2, $3)}'
  elif [ ${MINUTES} -gt 0 ]; then
    echo ${MINUTES} ${SECONDS} | awk '{printf("(%02dm.%02ds uptime)", $1, $2)}'
  elif [ ${SECONDS} -ge 0 ]; then
    echo ${SECONDS} | awk '{printf("(%02ds uptime)", $1)}'
  fi
}

get-tmp-datefile() {
  local FILE="$(get-tmp-watchfile "$@").success.date"

  touch "${FILE}"

  echo "${FILE}"
}

get-tmp-watchfile() {
  local INVALID_FILE_CHARS_REGEX="[^A-Za-z0-9\.-]"
  local URL="${1}"

  if [ $# -ne 1 ]; then
    >&2 echo "get-tmp-watchfile: Expected exactly one arg, got $#, arglist: $@"

    return 1
  elif [ "${URL}" = "" ]; then
    >&2 echo "get-tmp-watchfile: Missing the expected URL arg, got '${URL}'"

    return 1
  fi

  decho -n "get-tmp-watchfile: /tmp/watch.rss/"
  decho "${URL} | sed -E \"s/${INVALID_FILE_CHARS_REGEX}/./g;s/[\.]+/./g\""
  decho -n "get-tmp-watchfile: /tmp/watch.rss/"
  decho $(echo "${URL}" | sed -E "s/${INVALID_FILE_CHARS_REGEX}/./g;s/[\.]+/./g")
  echo -n "/tmp/watch.rss/"
  echo "${URL}" | sed -E "s/${INVALID_FILE_CHARS_REGEX}/./g;s/[\.]+/./g"
}

log-if() {
  local CONTEXT=
  local EXPECTED=1
  local VALUE=

  if [ $# -lt 4 ]; then
    >&2 echo "log-if: Not enough args $#, expected at least 4"

    return 1
  fi

  VALUE="${1}"
  CONTEXT="${2}"
  START_TIME="${3}"

  shift 3

  if [ "${VALUE}" -eq "${EXPECTED}" ]; then
    >&2 echo "${CONTEXT}" "$(2>&1 format-uptime "${START_TIME}")" "$@"
  fi
}

make-random-tmp-file() {
  local FILE="/tmp/watch.rss/random-${RANDOM}-${RANDOM}-${RANDOM}-${RANDOM}-${RANDOM}"

  mkdir -p "$(dirname "${FILE}")"

  touch "${FILE}"

  echo "${FILE}"
}

parse-equals() {
  # Omit the = from ARG_KEY
  local ARG_KEY="${1}"
  # ARG should be the full --example=value
  local ARG="${2}"

  echo "${ARG}" | sed "s/${ARG}=//"
}

process-url() {
  local CODE=
  local FILE=
  local FILTER=
  local KEEP=0
  local LINE=
  local PARSER=
  local PERIOD=
  local URL=

  local THIS_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
  . "${THIS_DIR}/parsers.sh"

  while [ $# -gt 0 ]; do
    case $1 in
      --keep)
        KEEP=1
        ;;
      --filter)
        FILTER="${2}"
        shift
        ;;
      --filter=*)
        FILTER="$(parse-equals --line "${1}")"
        ;;
      --line)
        LINE="${2}"
        shift
        ;;
      --line=*)
        PARSER="$(parse-equals --line "${1}")"
        ;;
      --parser)
        PARSER="${2}"
        shift
        ;;
      --parser=*)
        PARSER="$(parse-equals --parser "${1}")"
        ;;
      --period)
        PERIOD="${2}"
        shift
        ;;
      --period=*)
        PERIOD="$(parse-equals --period "${1}")"
        ;;
      --tmp-file)
        FILE="${2}"
        shift
        ;;
      --tmp-file=*)
        FILE="$(parse-equals --tmp-file "${1}")"
        ;;
      --url)
        URL="${2}"
        shift
        ;;
      --url=*)
        URL="$(parse-equals --url "${1}")"
        ;;
    esac

    shift
  done

  if [ "${URL}" = "" ]; then
    >&2 echo "process-url: Missing the expected URL arg, got '${URL}'"

    return 1
  fi

  decho "process-url: check-success-date --at-least-seconds '${PERIOD:-86400}' --url '${URL}' >&2"
  if ! check-success-date --at-least-seconds "${PERIOD:-86400}" --url "${URL}" >&2; then
    decho "process-url: exiting early because success date not up"

    return 0
  fi

  >&2 echo "process-url: Checking for changes to '${URL}'"

  if [ "${FILE}" = "" ]; then
    FILE="$(make-random-tmp-file)"
  fi

  if ! [ -f "${FILE}" ]; then
    >&2 echo "process-url: Expected ${FILE} to exist"

    return 1
  fi

  if [ "${PARSER}" = "" ]; then
    PARSER=default
  fi

  if ! type "${PARSER}" 2>/dev/null >&2; then
    >&2 echo "process-url: Expected parser ${PARSER} to exist"
  fi

  >&2 echo "process-url: curl -so '${FILE}' '${URL}'"
  curl -so "${FILE}" "${URL}" || echo -ne
  CODE=${PIPESTATUS[0]}

  if [ ${CODE} -eq 0 ]; then
    save-success-date --url "${URL}"

    ARGS=(${PARSER} --source-file "${FILE}")

    if [ "${FILTER}" != "" ]; then
      ARGS+=("--filter" "${FILTER}")
    fi

    eval "${ARGS[@]}"
  else
    >&2 echo "process-url: curl failure CODE=${CODE}"
  fi

  if [ ${KEEP} -ne 1 ]; then
    rm "${FILE}"
  fi

  return ${CODE}
}

save-success-date() {
  local FILE=
  local URL=

  while [ $# -gt 0 ]; do
    case $1 in
      --url)
        URL="${2}"
        shift
        ;;
      --url=*)
        URL="$(parse-equals --url "${1}")"
        ;;
    esac

    shift
  done

  if [ "${URL}" = "" ]; then
    >&2 echo "save-success-date: Missing the expected URL arg, got '${URL}'"

    return 1
  fi

  FILE="$(get-tmp-datefile "${URL}")"

  dt > "${FILE}" \
    || >&2 echo -ne
  CODE=${PIPESTATUS[0]}

  return "${CODE}"
}

if [[ "${BASH_SOURCE[0]}" == "$0" ]]; then
  eval "$@"
fi
